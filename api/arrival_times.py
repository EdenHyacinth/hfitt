import requests
from os import urandom
from flask_injector import inject
from json import JSONDecodeError
from app import redis_store
from utils.acquire_redlock import acquire_lock, check_lock

class Vehicles: 
    def __init__(self, json_res): 
        self.allVehicles =  set([x['vehicleId'] for x in json_res])
        self.len = len(self.allVehicles)
        self.chunk_size = 20

    def __iter__(self): 
        return self

    def __next__(self): 
        self.len = len(self.allVehicles)
        if self.len > 0:
            return [self.allVehicles.pop() for x in range(0, min(self.chunk_size, self.len))]
        else:
            raise StopIteration

class ArrivalTimes(object):
    def post(self, VIds: dict) -> dict:
        """
        Takes a list of VehicleIDs as input
        Returns an object of current information for that VehicleID.
        """

        vehicleIDs = VIds['id']
        if len(vehicleIDs) == 0:
            return {'error': 'No Vehicle IDs supplied'}, 400

        if len(vehicleIDs) > 1:
            vehiclePayload = '%2C'.join(vehicleIDs)
        else: 
            vehiclePayload = vehicleIDs[0]

        res = requests.get(url='https://api.tfl.gov.uk/Vehicle/' + vehiclePayload + '/Arrivals')
        
        if(res.ok is False):
            return {'error': 'API failed to reply'}, 502
        
        try: 
            json_res = res.json() 
        except JSONDecodeError: 
            return {'error': 'No JSON present, or JSON failed to decode'}, 502 
        
        # Write to Object & Redis
        ArrivalTimeData = [None] * len(json_res)
        redis_key_store = []
        idx = 1
        redis_pipe = redis_store.pipeline()
        redis_key_store = [] 
        for row in json_res:
            ArrivalTimeData.insert(
                idx, 
                {   'lineid' : row['lineId'], 
                    'vehicleId' : row['vehicleId'],
                    'arrivalLocation' : row['naptanId'],
                    'arrivalTimeInitial' : row['expectedArrival'], 
                    'destinationId' : row['destinationNaptanId']  if 'destinationNaptanId' in row else 'Unknown',
                    'hour' : 0,  
                    'date' : '1900-01-01'}
                )
            redis_key = ':'.join([
                row['lineId'], 
                row['vehicleId'],
                row['naptanId'],
                row['destinationNaptanId']  if 'destinationNaptanId' in row else 'Unknown']
            )

            redis_key_store.append(':'.join([
                row['lineId'], 
                row['vehicleId'],
                row['destinationNaptanId']]))

            redis_pipe.setnx(
                redis_key + ':initial',
                row['expectedArrival'] 
            )

            redis_pipe.set(
                redis_key + ':updated',
                row['expectedArrival'] 
            )
    
        redis_key_store = set(redis_key_store)  
        """
        After a key is expired, we can assume 
        there has been no activity for it for 
        15 minutes, and it can be retired to 
        cold storage. 
        """
        for redis_expiry_key in redis_key_store: 
            redis_pipe.set(
                    redis_expiry_key,
                    True, 
                    60 * 15)

        redis_lock = acquire_lock()
        if check_lock(redis_lock, redis_store, 0) is False:
            return {'error': 'Lock failed to acquire'}, 504 
        redis_pipe.execute()

        # Ignoring Pipe, execute a pull and return
        allLiveKeys = redis_store.get('allLiveKeys').split('|')
        if allLiveKeys == 'nil':
            allLiveKeys = allLiveKeys + redis_key_store
        else:
            allLiveKeys = redis_key_store
        redis_store.set('allLiveKeys', allLiveKeys)

        redis_lock.remove_lock(redis_store)

        return ArrivalTimeData, 200

class_instance = ArrivalTimes()
