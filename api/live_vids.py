import requests
from flask_injector import inject
from json import JSONDecodeError

class VehicleIds(object):
    def get(self) -> dict:
        """
        This returns a list of VehicleIDs as an array. 
        """
        res = requests.get(url='https://api.tfl.gov.uk/Mode/tube/Arrivals?count=-1')
        
        if(res.ok is False):
            return {'error': 'API failed to reply'}, 400
        
        try: 
            json_res = res.json() 
        except JSONDecodeError: 
            return {'error': 'No JSON present, or JSON failed to decode'}, 400 

        formatted_ids = list(set([x['vehicleId'] for x in json_res]))
        VIds = {'id' : formatted_ids}
        return VIds, 200
        

class_instance = VehicleIds()