import requests
from json import JSONDecodeError
from app import redis_store
from utils.acquire_redlock import acquire_lock, check_lock

class RouteDirection(object):
    def init(self):
        """
        Initialise a Redis Storage for all Routes
        """

        res = requests.get(url='https://api.tfl.gov.uk/Line/Mode/tube')
        if(res.ok is False):
            return {'error': 'API failed to reply'}, 400
        
        try: 
            json_res = res.json() 
        except JSONDecodeError: 
            return {'error': 'No JSON present, or JSON failed to decode'}, 400 

        LineIDs = [line['id'] for line in json_res]

        if(redis_store.exists(LineIDs[0] + ':Outbound:OrderedStops')):
            return {'warning': 'Redis already initialised'}

        # Retrieve the normal values to store in Redis 
        redis_pipe = redis_store.pipeline()
        for LineID in LineIDs:
            res = requests.get(url='https://api.tfl.gov.uk/Line/' + LineID + '/Route/Sequence/outbound?serviceTypes=Regular&excludeCrowding=true')

            if(res.ok is False):
                return {'error': 'API failed to reply'}, 400
            
            try: 
                json_res = res.json() 
            except JSONDecodeError: 
                return {'error': 'No JSON present, or JSON failed to decode'}, 400 
            
            redis_pipe.set(
                LineID + ':' + ':Outbound:OrderedStops',
                '|'.join([','.join(Route) for Route in [Route['naptanIds'] for Route in json_res['orderedLineRoutes']]])
            )    
        
        redis_lock = acquire_lock()
        if(check_lock(redis_lock, redis_store, 0) == False):
            return {'error': 'Lock failed to acquire'}, 504 
        redis_pipe.execute()

    def get_route(self,LineID) -> dict:
        LineID = LineID.lower()
        LineID = LineID.replace(' & ', '-')
        return redis_store.get(LineID + ':Outbound:OrderedStops')

    def check_outbound(self, LineID, stationA, stationB) -> bool:
        """
        Return the direction of travel between two points.
        Returns True if it is going L->R (Outbound) on the Tube directions, or
        False if not. 
        Returns False if it is not in the list. 
        """
        routes = self.get_route(LineID).split('|')
        for route_vector in routes:
            route_vector = route_vector.split(',')
            try:
                Outbound = route_vector.index(stationA) < route_vector.index(stationB)
            except ValueError:
                continue 
        return Outbound 