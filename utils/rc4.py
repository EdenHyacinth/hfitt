class rc4: 
    def __init__(self, key):
        """
        RC4 Pseudo-Random Algorithm
        key = 'US Secrets'
        input_key = [ord(c) for c in key]
        keystream = rc4(input_key)
        plaintext = 'Oh no, the Russkis'
        ["%02X" % (ord(c) ^ keystream.__next__() for c in plaintext)]
        """
        # Standard RC4
        keylength = len(key)
        self.s = [i for i in range(256)]
        self.i = 0
        self.j = 0
        j = 0
        for i in range(256):
            j = (j + self.s[i] + key[i % keylength]) % 256
            self.s[i], self.s[j] = self.s[j], self.s[i] 

    def __iter__(self):
        return self 

    def __next__(self):
        """
        Pseudo-random generation algorithm (PRGA)
        """
        self.i = (self.i + 1) % 256
        self.j = (self.j + self.s[self.i]) % 256 
        self.s[self.i], self.s[self.j] = self.s[self.j], self.s[self.i] 
        return self.s[(self.s[self.i] + self.s[self.j]) % 256]