from os import urandom
from app import redis_store
from utils.rc4 import rc4
from time import sleep

def check_lock(lock, redis, timeouts, sleep_time = 0):
    if(timeouts > 3):
            return False
    if(lock.check_lock(redis)):
        return True
    sleep(sleep_time * 2)
    check_lock(lock, redis, timeouts + 1, (sleep_time + 30))

class acquire_lock: 
    def __init__(self): 
        self.seed = urandom(20)
        self.plaintext = urandom(20)
        keystream = rc4(self.seed)
        self.key = ''.join(["%02X" % (c ^ keystream.__next__()) for c in self.plaintext])

    def check_lock(self, redis: redis_store):
        redis.set(
            name = 'redis_lock', 
            value = self.key, 
            nx = True, 
            px = 30000 
        )
        if(redis.get(name = 'redis_lock') == self.key):
            return True
        return False

    def remove_lock(self, redis: redis_store):
        if(redis.get(name = 'redis_lock') == self.key):
            redis.delete(name = 'redis_lock')
            return True 
        return False
