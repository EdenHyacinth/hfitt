import os
import connexion
import redis

from flask import Flask 
from flask_sqlalchemy import SQLAlchemy


from connexion.resolver import RestyResolver

redis_store = redis.StrictRedis('redis', port = 6379)

if __name__ == '__main__': 
    app = connexion.FlaskApp(__name__, specification_dir = 'swagger/')
    app.add_api('indexer.yml', resolver = RestyResolver('api'))
    app.app.config.from_object('config.ProductionConfig')
    app.run(host = '0.0.0.0', port = 9090)