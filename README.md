# hfitt (How *functional* is the Tube?)

# Overview
**This project is in progress**

The end goal is to write a series of microservices to visualise the severity of issues within the London Underground. 

Status:
- [x] Can pull formatted Tube Arrival Times into Redis 
- [ ] Arrival Times are sent to postgreSQL when no longer live
- [ ] postgreSQL query for identifying tube delays 
- [ ] Initial model for estimating how delays will develop
- [ ] Web interace to visualise current delays & predicted delays

# Installation
Python3 and Docker are required as a base. The other requirements will be 
installed automatically within Docker. 

Redis-cli is recommended while doing any work on this. 

```bash
docker-compose build 
docker-compose up
```

Afterwards you can verify if it's up by prodding the endpoint /v1.0/live_vids/ at the IP Docker provides. 

